package com.jaysyko.latenight;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String[] locationArray = {
                "Fortinos - 11:00 PM",
                "Yorkdale Mall - 24HRS",
                "Swimming Pool - CLOSED",
                "Lost and Found - 2 AM",
                "Fortinos - 11:00 PM",
                "Yorkdale Mall - 24HRS",
                "Swimming Pool - CLOSED",
                "Lost and Found - 2 AM",
                "Fortinos - 11:00 PM",
                "Yorkdale Mall - 24HRS",
                "Swimming Pool - CLOSED",
                "Lost and Found - 2 AM",
                "Fortinos - 11:00 PM",
                "Yorkdale Mall - 24HRS",
                "Swimming Pool - CLOSED",
                "Lost and Found - 2 AM",
                "Fortinos - 11:00 PM",
                "Yorkdale Mall - 24HRS",
                "Swimming Pool - CLOSED",
                "Lost and Found - 2 AM"
        };

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        List<String> locations = new ArrayList<String>(
                Arrays.asList(locationArray)
        );

        ArrayAdapter<String> locationAdapter = new ArrayAdapter<String>(
                getActivity(),
                R.layout.list_item_location,
                R.id.tvListItems, locationArray
        );

        ListView locationListView = (ListView) rootView.findViewById(
                R.id.lv_Locations
        );

        locationListView.setAdapter(locationAdapter);
        return rootView;
    }
}
